class ApiConfig < ConfigReader
  self.config_file = File.join(Rails.root, "config/api_config.yml")
end
