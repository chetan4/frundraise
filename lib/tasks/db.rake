namespace :db do
  desc "Delete all data"
  task :delete_all => :environment do
    User.delete_all
    Charity.delete_all
    Fundraiser.delete_all
  end
end