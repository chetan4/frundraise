def current_uri
  URI.parse(current_url).request_uri
end