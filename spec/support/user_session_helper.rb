module UserSessionHelper
  def sign_in(email, password)
    visit new_user_session_path
    within '.new_user_session_form' do
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      click_button "Sign in"
    end
    page.should have_content "Signed in successfully"
    # save_and_open_page
  end
end