# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :charity do
    name "MyString"
    description "MyString"
    logo "MyString"
    tax_id "MyString"
    ein "MyString"
    address "Blah blah Street\nSomecity CA 55555"
    phone "555 555 5555"

    factory :red_cross_charity do
      name 'American Red Cross'
      website 'http://www.redcross.org'
      logo 'http://www.redcross.org/images/redcross-logo.png'
      tax_id '53-0196605'
      ein '95-1643965'
      address "2025 E Street, NW\nWashington, DC 20006"
      phone '202-303-5214'
    end
  end
end
