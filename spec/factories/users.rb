# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "email_#{n}@example.com"}
    password "UpperL0wer123456"
    password_confirmation { password }
  end
end