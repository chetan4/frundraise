# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :training_log do
    miles "1"
    fundraiser nil
  end
end
