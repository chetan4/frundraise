# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :fundraiser do
    name "MyString"
    goal "MyString"
    image "MyString"
    description "MyText"
    story "MyText"
  end
end
