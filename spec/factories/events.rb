# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    name "Boston Marathon"
    happens_on { 1.month.from_now }
  end
end
