# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :donation do
    fixed_amount "9.99"
    per_mile_amount "9.99"
    max_per_mile_amount "9.99"
    user nil
    stripe_token "MyString"
    fundraiser nil
  end
end
