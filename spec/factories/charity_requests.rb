# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :charity_request do
    charities "MyText"
    phone_number "MyString"
    email "MyString"
    event_id 1
    name "MyString"
  end
end
