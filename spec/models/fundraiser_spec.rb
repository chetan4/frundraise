require 'spec_helper'

describe Fundraiser do
  it { should belong_to :user }
  it { should belong_to :charity }
end
