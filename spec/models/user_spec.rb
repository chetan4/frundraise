require 'spec_helper'

describe User do
  it { should have_db_column 'first_name' }
  it { should have_db_column 'last_name' }
  it { should have_many :fundraisers }

  describe "#name" do
    before(:each) do
      @user = build :user, email: 'jimmy@example.com'
    end

    context 'first and name present'  do
      it "return combined name" do
        @user.assign_attributes first_name: 'Jimmy', last_name: 'Chance'
        @user.name.should == 'Jimmy Chance'
      end
    end

    context 'first name present, last name missing'  do
      it "return first name" do
        @user.assign_attributes first_name: 'Jimmy', last_name: nil
        @user.name.should == 'Jimmy'
      end
    end

    context 'last name present, first name missing'  do
      it "return first name" do
        @user.assign_attributes first_name: nil, last_name: 'James'
        @user.name.should == 'James'
      end
    end

    context 'first and last name missing'  do
      it "derive name from email" do
        @user.assign_attributes first_name: nil, last_name: nil
        @user.name.should == 'Jimmy'
      end
    end
  end
end
