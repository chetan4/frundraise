require 'spec_helper'
require 'pry'
describe Donation do
  it "predicate pledge donation" do

    charity = create(:red_cross_charity)
    event = create(:event)
    event.happens_on = Date.parse("2014/07/17")
    event.save
    user = create(:user)
    fundraiser = create(:fundraiser)
    fundraiser.user_id = user.id
    fundraiser.event_id = event.id
    fundraiser.charity_id = charity.id
    fundraiser.save
    sarah = create(:user)
    sarah_donation = sarah.donations.create(:created_at=>"2014/04/21",:per_mile_amount=>"0.10",:fundraiser_id=>fundraiser.id)
    training_log_1 = fundraiser.training_logs.create(:created_at=>"2014/04/19",:miles=>4,:training_date=>"2014/04/18")
    training_log_2 = fundraiser.training_logs.create(:created_at=>"2014/04/29",:miles=>6,:training_date=>"2014/04/28")
    training_log_3 = fundraiser.training_logs.create(:created_at=>"2014/05/3",:miles=>9,:training_date=>"2014/05/02")
    training_log_4 = fundraiser.training_logs.create(:created_at=>"2014/05/10",:miles=>8,:training_date=>"2014/05/09")
    training_log_5 = fundraiser.training_logs.create(:created_at=>"2014/05/22",:miles=>10,:training_date=>"2014/05/17")
    training_log_6 = fundraiser.training_logs.create(:created_at=>"2014/05/29",:miles=>14,:training_date=>"2014/05/28")
    training_log_7 = fundraiser.training_logs.create(:created_at=>"2014/06/03",:miles=>22,:training_date=>"2014/06/02")
    training_log_8 = fundraiser.training_logs.create(:created_at=>"2014/06/15",:miles=>16,:training_date=>"2014/06/14")
    training_log_9 = fundraiser.training_logs.create(:created_at=>"2014/06/27",:miles=>14,:training_date=>"2014/06/26")
    training_log_9.save(:validate=>false)
    training_log_10 = fundraiser.training_logs.create(:created_at=>"2014/07/15",:miles=>8,:training_date=>"2014/07/14")
    training_log_10.save(:validate=>false)
    # training_log_11 = fundraiser.training_logs.create(:created_at=>"2014/07/22",:miles=>2,:training_date=>"2014/07/14")
    # training_log_11.save(:validate=>false)
    # total_miles = sarah_donation.get_total_miles_for_months(Date.today+60.days)
    nicole = create(:user)
    nicole_donation = nicole.donations.create(:created_at=>"2014/05/07",:per_mile_amount=>"0.25",:fundraiser_id=>fundraiser.id,:max_per_mile_amount=>'10')
    # binding.pry
    nicole_donation.make_pledge_donations
    # total_miles = nicole_donation.get_total_miles_for_months(Date.today+60.days)
  end
end
