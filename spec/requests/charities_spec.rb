require 'spec_helper'

describe "CharitiesController" do
  describe "index" do
    describe "json request" do
      before(:each) do
        @red_cross_charity = create :red_cross_charity
        @random_charity = create :charity, name: 'American Pie Society'
      end

      context 'no search query' do
        it "should return id, name, website, address, phone, logo_url" do
          visit charities_path(format: :json)
          page.body.should == [@red_cross_charity, @random_charity].to_json
        end
      end

      context 'with search query' do
        it "should return id, name, website, address, phone, logo_url" do
          pending "Charity search will work after full text is setup"
          # visit charities_path(search: 'American Red Cross', format: :json)
          visit charities_path(search: URI.escape('american red cross'), format: :json)
          page.body.should == [@red_cross_charity].to_json
        end
      end      


    end
  end
end