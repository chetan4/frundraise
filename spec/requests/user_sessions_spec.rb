# save_and_open_page
require 'spec_helper'

describe 'User Session' do
  before(:each) do
    visit root_path
  end

  context 'new' do
    before(:each) do
      @user = create :user, password: 'SecretPassc0de'
    end
    it 'sign in user' do
      within('.session_links') { click_link 'Sign in' }
      page.should have_css 'form#new_user'
      sign_in @user.email, 'SecretPassc0de'
      page.should have_content "Signed in successfully"
      page.should have_content @user.email
    end
  end

  context 'destroy' do
    before(:each) do
      @user = create :user, password: 'SecretPassc0de'
      sign_in @user.email, 'SecretPassc0de'
    end
    it 'sign out user' do
      within('.session_links') { click_link 'Sign out' }
      page.should have_content "Signed out successfully"
      page.should_not have_link 'Sign out'
    end
  end

end