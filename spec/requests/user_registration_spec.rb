# save_and_open_page
require 'spec_helper'

describe "User Registration" do
  describe "new" do
    before(:each) do
      visit new_user_registration_path
      within '.new_user_registration_form' do
        fill_in 'user_first_name', with: 'Joe'
        fill_in 'user_last_name', with: 'Smith'
        fill_in 'Email', with: 'joe@example.com'
        fill_in 'Password', with: 'Pss0word123'
        fill_in 'Password confirmation', with: 'Pss0word123'
        click_button 'Sign up'
      end
    end
    it 'create user' do
      page.should have_content 'You have signed up successfully'
    end

    it 'sign in user' do
      page.should have_link 'Sign out'
    end
  end
end