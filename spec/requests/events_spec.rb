require 'spec_helper'

describe "EventsController" do
  describe "index" do
    describe "json request" do
      before(:each) do
        @ny_marathon_event = create :event, name: 'New York Marathon'
        @random_event = create :event
      end

      context 'no search query' do
        it "should return id, name, date" do
          visit events_path(format: :json)
          page.body.should == [@ny_marathon_event, @random_event].to_json
        end
      end

      context 'with search query' do
        it "should return id, name, date" do
          pending "Event search will work after full text is setup"
          visit events_path(query: URI.escape('new+york'), format: :json)
          page.body.should == [@ny_marathon_event].to_json
        end
      end      


    end
  end
end