require 'spec_helper'

describe 'Home#index' do
  it 'loads' do
    visit root_path
  end

  context 'guest user' do
    it 'redirects to authentication page' do
      visit root_path
      click_link 'Start a Fundraiser'
      current_uri.should == new_user_session_path
    end
  end

  context 'signed in user' do
    before(:each) do
      user = create :user, password: 'ppass12223'
      sign_in user.email, 'ppass12223'
    end
    it 'links to new frundraiser page' do
      visit root_path
      click_link 'Start a Fundraiser'
      current_path.should == new_fundraiser_path
    end
  end

end