require 'spec_helper'

describe "Fundraisers" do
  describe "new", js: true do
    before(:each) do
      @charity = create :charity, name: "American Breast Cancer Society"
      @event = create :event, name: "Boston Marathon"
    end

    it "allows me to create a new Fundraiser" do
      @user = create :user, password: 'UpperLower123456'
      sign_in @user.email, 'UpperLower123456'
      visit new_fundraiser_path
      fill_in "fundraiser_name", with: "Nick Morrison"
      fill_in "fundraiser_goal", with: "100.00"
      attach_file "fundraiser_image", Rails.root.join("spec/fixtures/fundraiser_image.jpg")
      fill_in "fundraiser_description", with: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae luctus felis, ut posuere lorem. Aliquam scelerisque a ante nec convallis. Maecenas nec tellus euismod ligula rutrum consequat."      
      fill_in "Charity", with: "American Breast Cancer"
      
      page.should have_css('.charity')
      all('.charity').count.should == 1
      first('.charity').click_link "Use This Charity"
      find('#fundraiser_charity_id', visible: false).value.should == @charity.id.to_s

      fill_in "Event", with: "Boston"

      page.should have_selector('.event')
      all('.event').count.should == 1
      first('.event').click_link "Use This Event"
      find('#fundraiser_event_id', visible: false).value.should == @event.id.to_s


      click_button "Start My Fundraiser"

      # within(".fundraiser_name") { page.should have_content 'Nick Morrison' }
      # page.should have_content 'Nick Morrison'
    end
  end
end