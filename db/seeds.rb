require 'factory_girl'

def user
  @user ||= User.where(email: 'admin@example.com', first_name: 'Admin', last_name: 'User').first
  @user ||= FactoryGirl.create :user, email: 'admin@example.com', password: 'Password1'
end

def charity
  @charity ||= Charity.where(name: 'American Red Cross').first
  @charity ||= FactoryGirl.create :red_cross_charity, user: user
end

def event
  @event ||= Event.where(name: 'Boston Marathon').first
  @event ||= FactoryGirl.create :event, name: 'Boston Marathon'
end

def fundraiser
  @fundraiser ||= Fundraiser.where(name: 'Think Of The Kids').first
  @fundraiser ||= FactoryGirl.create :fundraiser, name: "Think Of The Kids", charity: charity, user: user, event: event
end

if User.all.empty?
  user
end

if Charity.all.empty?
  charity
end

if Event.all.empty?
  event
end

if Fundraiser.all.empty?
  fundraiser
end

