class CreateTrainingLogs < ActiveRecord::Migration
  def change
    create_table :training_logs do |t|
      t.decimal :miles
      t.references :fundraiser, index: true

      t.timestamps
    end
  end
end
