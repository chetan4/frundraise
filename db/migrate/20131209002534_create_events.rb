class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.date :happens_on

      t.timestamps
    end
  end
end
