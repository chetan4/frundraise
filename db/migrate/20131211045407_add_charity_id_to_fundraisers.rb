class AddCharityIdToFundraisers < ActiveRecord::Migration
  def change
    add_reference :fundraisers, :charity, index: true
  end
end
