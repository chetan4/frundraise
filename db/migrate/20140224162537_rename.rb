class Rename < ActiveRecord::Migration
  def change
    rename_table :activity_notifications, :activities
  end
end
