class AddIndexToActivityNotificationData < ActiveRecord::Migration
  def change
    add_index :activity_notifications, :activity_type
    add_index :activity_notifications, :meta_data, using: :gin
  end
end
