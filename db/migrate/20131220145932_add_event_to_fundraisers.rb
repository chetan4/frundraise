class AddEventToFundraisers < ActiveRecord::Migration
  def change
    add_reference :fundraisers, :event, index: true
  end
end
