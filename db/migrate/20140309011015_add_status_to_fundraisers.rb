class AddStatusToFundraisers < ActiveRecord::Migration
  def change
    add_column :fundraisers, :status, :string, default: 'draft'
  end
end
