class AddTrainingDateToTrainingLogs < ActiveRecord::Migration
  def change
    add_column :training_logs, :training_date, :date
  end
end
