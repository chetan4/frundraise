class CreateActivityNotifications < ActiveRecord::Migration
  def change
    create_table :activity_notifications do |t|
      t.string :activity_type
      t.hstore :meta_data

      t.timestamps
    end
  end
end
