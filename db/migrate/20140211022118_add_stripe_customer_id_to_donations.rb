class AddStripeCustomerIdToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :stripe_customer_id, :string
  end
end
