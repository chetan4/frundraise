class AddUserIdToCharities < ActiveRecord::Migration
  def change
    add_reference :charities, :user, index: true
  end
end
