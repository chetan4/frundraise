class AddUserIdToFundraisers < ActiveRecord::Migration
  def change
    add_column :fundraisers, :user_id, :integer
    add_index :fundraisers, :user_id
  end
end
