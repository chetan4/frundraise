class AddTotalDonationsToFundraisers < ActiveRecord::Migration
  def change
    add_column :fundraisers, :total_donations, :decimal, default: 0
  end
end
