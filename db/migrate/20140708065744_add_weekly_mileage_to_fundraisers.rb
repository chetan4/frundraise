class AddWeeklyMileageToFundraisers < ActiveRecord::Migration
  def change
  	add_column :fundraisers, :weekly_mileage, :integer
  end
end
