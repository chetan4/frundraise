class CreateCharities < ActiveRecord::Migration
  def change
    create_table :charities do |t|
      t.string :name
      t.string :description
      t.string :logo
      t.string :tax_id
      t.string :ein

      t.timestamps
    end
  end
end
