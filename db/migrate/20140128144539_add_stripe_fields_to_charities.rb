class AddStripeFieldsToCharities < ActiveRecord::Migration
  def change
    add_column :charities, :stripe_access_token, :string
    add_column :charities, :stripe_user_id, :string
    add_column :charities, :stripe_publishable_key, :string
  end
end
