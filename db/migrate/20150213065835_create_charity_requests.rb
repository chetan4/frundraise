class CreateCharityRequests < ActiveRecord::Migration
  def change
    create_table :charity_requests do |t|
      t.text :charities
      t.string :phone_number
      t.string :email
      t.integer :event_id
      t.string :name

      t.timestamps
    end
  end
end
