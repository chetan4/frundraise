class AddTotalToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :total, :decimal
  end
end
