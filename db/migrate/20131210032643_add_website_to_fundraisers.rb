class AddWebsiteToFundraisers < ActiveRecord::Migration
  def change
    add_column :fundraisers, :website, :string
  end
end
