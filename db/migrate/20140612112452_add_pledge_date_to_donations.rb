class AddPledgeDateToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :pledge_date, :date
  end
end
