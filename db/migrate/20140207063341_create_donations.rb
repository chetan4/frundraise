class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.decimal :fixed_amount
      t.decimal :per_mile_amount
      t.decimal :max_per_mile_amount
      t.references :user, index: true
      t.string :stripe_token
      t.references :fundraiser, index: true

      t.timestamps
    end
  end
end
