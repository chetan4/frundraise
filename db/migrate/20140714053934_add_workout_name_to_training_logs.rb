class AddWorkoutNameToTrainingLogs < ActiveRecord::Migration
  def change
  	add_column :training_logs, :workout_name, :string
  end
end
