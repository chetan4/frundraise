class CreateFundraisers < ActiveRecord::Migration
  def change
    create_table :fundraisers do |t|
      t.string :name
      t.string :goal
      t.string :image
      t.text :description
      t.text :story

      t.timestamps
    end
  end
end
