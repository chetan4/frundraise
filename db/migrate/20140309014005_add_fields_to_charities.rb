class AddFieldsToCharities < ActiveRecord::Migration
  def change
    add_column :charities, :contact, :string
    add_column :charities, :email, :string
    add_column :charities, :other, :string
  end
end
