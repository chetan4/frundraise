class AddAddressAndPhoneToCharities < ActiveRecord::Migration
  def change
    add_column :charities, :address, :string
    add_column :charities, :phone, :string
  end
end
