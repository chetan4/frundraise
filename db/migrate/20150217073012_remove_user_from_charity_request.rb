class RemoveUserFromCharityRequest < ActiveRecord::Migration
  def up
  	remove_column :charity_requests, :email
  	remove_column :charity_requests, :phone_number
  	remove_column :charity_requests, :name
  	add_column :charity_requests, :user_id,:integer
  	add_column :users, :phone_number, :string
  end
end
