class AddColumnFeePercentageToCharity < ActiveRecord::Migration
  def change
    add_column :charities, :fee_percentage, :float ,default: 5.0
  end
end
