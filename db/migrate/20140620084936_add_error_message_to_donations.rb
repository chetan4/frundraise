class AddErrorMessageToDonations < ActiveRecord::Migration
  def change
    add_column :donations, :error_message, :text
  end
end
