class CreateEventCharities < ActiveRecord::Migration
  def change
    create_table :event_charities do |t|
      t.integer :event_id
      t.integer :charity_id

      t.timestamps
    end

    remove_column :charities,:event_id
  end
end
