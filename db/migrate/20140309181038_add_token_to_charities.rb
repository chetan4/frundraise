class AddTokenToCharities < ActiveRecord::Migration
  def change
    add_column :charities, :token, :string
  end
end
