# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150217073012) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "activities", force: true do |t|
    t.string   "activity_type"
    t.hstore   "meta_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["activity_type"], name: "index_activities_on_activity_type", using: :btree
  add_index "activities", ["meta_data"], name: "index_activities_on_meta_data", using: :gin

  create_table "charities", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "logo"
    t.string   "tax_id"
    t.string   "ein"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "website"
    t.string   "address"
    t.string   "phone"
    t.string   "stripe_access_token"
    t.string   "stripe_user_id"
    t.string   "stripe_publishable_key"
    t.integer  "user_id"
    t.string   "status",                 default: "new"
    t.string   "contact"
    t.string   "email"
    t.string   "other"
    t.string   "token"
    t.float    "fee_percentage",         default: 5.0
  end

  add_index "charities", ["user_id"], name: "index_charities_on_user_id", using: :btree

  create_table "charity_requests", force: true do |t|
    t.text     "charities"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "donations", force: true do |t|
    t.decimal  "fixed_amount"
    t.decimal  "per_mile_amount"
    t.decimal  "max_per_mile_amount"
    t.integer  "user_id"
    t.string   "stripe_token"
    t.integer  "fundraiser_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "stripe_customer_id"
    t.decimal  "total"
    t.date     "pledge_date"
    t.integer  "parent_id"
    t.text     "error_message"
  end

  add_index "donations", ["fundraiser_id"], name: "index_donations_on_fundraiser_id", using: :btree
  add_index "donations", ["user_id"], name: "index_donations_on_user_id", using: :btree

  create_table "event_charities", force: true do |t|
    t.integer  "event_id"
    t.integer  "charity_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "name"
    t.date     "happens_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", force: true do |t|
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fundraisers", force: true do |t|
    t.string   "name"
    t.string   "goal"
    t.string   "image"
    t.text     "description"
    t.text     "story"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "website"
    t.integer  "charity_id"
    t.integer  "event_id"
    t.decimal  "total_donations", default: 0.0
    t.string   "status",          default: "draft"
    t.integer  "weekly_mileage"
  end

  add_index "fundraisers", ["charity_id"], name: "index_fundraisers_on_charity_id", using: :btree
  add_index "fundraisers", ["event_id"], name: "index_fundraisers_on_event_id", using: :btree
  add_index "fundraisers", ["user_id"], name: "index_fundraisers_on_user_id", using: :btree

  create_table "training_logs", force: true do |t|
    t.decimal  "miles"
    t.integer  "fundraiser_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "training_date"
    t.string   "workout_name"
  end

  add_index "training_logs", ["fundraiser_id"], name: "index_training_logs_on_fundraiser_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "provider"
    t.string   "uid"
    t.string   "role"
    t.string   "phone_number"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["role"], name: "index_users_on_role", using: :btree

end
