# config/unicorn.rb
worker_processes 3
timeout 30
preload_app true

before_fork do |server, worker|

  Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end
  @sidekiq_pid ||= spawn("bundle exec sidekiq -C config/sidekiq.yml")
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|

  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to sent QUIT'
  end
  redis_url =  ENV['REDIS_PROVIDER']
  
  Sidekiq.configure_client do |config|
    config.redis = { :url => redis_url,:size => 2 }
  end

  Sidekiq.configure_server do |config|
    # The config.redis is calculated by the 
    # concurrency value so you do not need to 
    # specify this. For this demo I do 
    # show it to understand the numbers
    config.redis = { :url => redis_url, :size => 4 }
  end

  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end