require 'sidekiq/web'
Frundraise::Application.routes.draw do
  mount RailsAdmin::Engine => '/super_admin', as: 'rails_admin'
  devise_for :users, 
    path_names: { sign_in: "login", sign_out: "logout" },
    controllers: { omniauth_callbacks: "omniauth_callbacks",registrations: 'registrations' }

  get 'dashboard' => 'dashboard#index', as: :dashboard

  resources :training_logs

  resources :fundraisers do
    member do
      get :publish, as: :publish
    end

    resources :donations
  end

  resources :charities do
    collection do
      get :info, as: :info
      get :my, as: :my
    end

    member do
      get :connect_with_stripe, as: :connect_with_stripe
      get :stripe_connected, as: :stripe_connected
      post :suspend
      post :unsuspend
      get :invite
      post :send_invite
      get :claim
      post :merge_charity
      post :own
    end
  end

  resources :donations

  resources :events do
    collection do 
      get :confirmation
    end
  end
  resources :users
  resources :feedbacks
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  get 'terms' => 'pages#terms'
  get 'about' => 'pages#about'
  get 'how-it-works' => 'pages#how-it-works'
   Sidekiq::Web.use Rack::Auth::Basic do |username, password|
      username == 'admin' && password == 'admin123'
  end
  mount Sidekiq::Web => '/sidekiq_workers'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
