
require 'sidekiq'
require 'autoscaler/sidekiq'
require 'autoscaler/heroku_scaler'


if Rails.env.production?
  redis_url =  ENV['REDIS_PROVIDER'] 


  heroku = nil
  if ENV['HEROKU_APP']
    heroku = Autoscaler::HerokuScaler.new
  end

  Sidekiq.configure_client do |config|
    if heroku
      config.client_middleware do |chain|
        chain.add Autoscaler::Sidekiq::Client, 'default' => heroku
      end
    end
  end

  Sidekiq.configure_server do |config|
    
    config.server_middleware do |chain|
      if heroku
        p "[Sidekiq] Running on Heroku, autoscaler is used"
        chain.add(Autoscaler::Sidekiq::Server, heroku, 60) # 60 seconds timeout
      else
        p "[Sidekiq] Running locally, so autoscaler isn't used"
      end
    end
  end



  Sidekiq.configure_client do |config|
    config.redis = { :url => redis_url,:size => 1 }
  end

  Sidekiq.configure_server do |config|
    # The config.redis is calculated by the 
    # concurrency value so you do not need to 
    # specify this. For this demo I do 
    # show it to understand the numbers
    config.redis = { :url => redis_url, :size => 38 }
  end
end