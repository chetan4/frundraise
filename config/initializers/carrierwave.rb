CarrierWave.configure do |config|

  fog_credentials = { provider: 'AWS' }
  fog_credentials[:aws_access_key_id] = ENV['S3_KEY'] ? ENV['S3_KEY'] : ApiConfig.S3_KEY
  fog_credentials[:aws_secret_access_key] = ENV['S3_SECRET'] ? ENV['S3_SECRET'] : ApiConfig.S3_SECRET
  fog_credentials[:region] = ENV['S3_REGION'] ? ENV['S3_REGION'] : ApiConfig.S3_REGION
  config.fog_credentials = fog_credentials

  config.fog_directory = ENV['S3_BUCKET_NAME'] ? ENV['S3_BUCKET_NAME'] : ApiConfig.S3_BUCKET_NAME
  # config.fog_host = "#{ENV['S3_ASSET_URL']}/#{ENV['S3_BUCKET_NAME']}"
 
  # For testing, upload files to local `tmp` folder.
  if Rails.env.test? || Rails.env.cucumber?
    config.storage = :file
    config.enable_processing = false
    config.root = "#{Rails.root}/tmp"
  elsif Rails.env.development?
    config.storage = :file
    config.root = "#{Rails.root}/tmp"
  else
    config.storage = :fog
  end
 
  # To let CarrierWave work on heroku
  config.cache_dir = "#{Rails.root}/tmp/uploads"
 
  
end