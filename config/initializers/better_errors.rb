if defined? BetterErrors
  BetterErrors::Middleware.allow_ip! '192.168.178.0/24'
end

BetterErrors.editor = :sublime if defined? BetterErrors