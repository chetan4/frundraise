require 'uri'

class CharitiesController < ApplicationController

  before_filter :authenticate_user!, only: [:new, :edit, :create, :delete, :my, :claim,:destroy,:connect_with_stripe]
  layout 'dashboard', only: [:my, :index]

  def index
    
    respond_to do |format|
      format.html do
        return redirect_to root_path, alert: "You are not authorized to view this page." unless user_signed_in? # and current_user.admin?
        
        if current_user.admin?
          @active_charities = Charity.active
          @inactive_charities = Charity.inactive
        end
              
        @my_charities = current_user.charities
        
      end
    
      format.json do
        charities = Charity.arel_table
        search_query = URI.unescape(params[:query]).gsub('+', ' ') if params[:query]
        @charities = Charity.where(charities[:name].matches("%#{search_query}%")) if search_query
        render json: @charities.to_json
      end
    end
  end

  def show
    session[:charity_id_for_stripe_connect] = params[:id]
    @charity = load_charity
    @fundraisers = @charity.active_fundraisers
    @can_edit = user_signed_in? && current_user.id == @charity.user_id
  end

  def new
    @charity = Charity.new
  end

  def edit
    @charity = load_charity
    raise ActiveRecord::RecordNotFound unless current_user.admin? or current_user.id == @charity.user_id
  end

  def claim
    @charity = load_charity
    @token = params[:token]
    if @token.blank? or @charity.token != @token
      redirect_to root_path, notice: "Error: The token you utilized while trying to claim a charity is invalid."
    end
  end

  def own
    @charity = load_charity
    if @charity.token == params[:token]
      @charity.user = current_user
      @charity.save
      redirect_to [:connect_with_stripe, @charity], notice: "Congratulations. You're now the owner of this charity. Your next step is to connect with Stripe."
    else
      redirect_to root_path, notice: "Error: The token you utilized while trying to claim a charity is invalid."
    end
  end

  def invite
    raise ActiveRecord::RecordNotFound unless current_user.admin?
    @charity = load_charity
    @message = "Hi#{@charity.contact ? " #{@charity.contact}" : ''}, \n\n#{@charity.fundraisers.first.user.name} is training for the #{@charity.fundraisers.first.event.name} and wants to start a fundraiser to benefit you on Frundraise. Frundraise is a fundraising website for runners, cyclists, triathletes, and all other endurance athletes who want to raise money for charity as part of their training for an event. Before #{@charity.fundraisers.first.user.name} can start raising money for you, we need you to set up your profile on Frundraise so that you can start accepting donations. Follow the link below to get started.\n\n#{claim_charity_url(@charity, token: @charity.token)}\n\nThe Frundraise Team\nhttp://www.frundraise.com"
    @athlete = @charity.fundraisers.first.user.name
  end

  def send_invite

    raise ActiveRecord::RecordNotFound unless current_user.admin?
    @body = params[:body]
    @recipient = params[:recipient]
    @athlete = params[:athlete]
    @charity = Charity.find(params[:id])
    if @recipient.blank?
       redirect_to invite_charity_path(@charity),notice: "Recipient can't be empty"
    else
      CharityMailer.new_charity_invite(@body, @recipient, @athlete).deliver
      redirect_to charities_path, notice: "Your invite was sent."
    end  
  end

  def info
    # @charities = Charity.order(created_at: :desc).sort_by {|x| x.raised}.reverse
    charity_ids = [11, 35, 56, 94, 100]
    @charities = Charity.where("id IN (?)", charity_ids).reverse!
    if @charities.blank?
      @names = ["Ronald McDonald House Charities of Central Ohio", "Bicycles For Humanity", "Tri 4 Schools", "Friends of Amani US", "Save the Colorado"]
      @charity_names = @names.map(&:downcase)
      @charities = Charity.where("lower(name) IN (?)", @charity_names).reverse!
    end
    
    @charities = @charities.insert(2, nil)
    @charities_slices = @charities.each_slice(3)
  end

  def create
    @charity = current_user.charities.new(charity_params)
    if @charity.save
      session[:charity_id_for_stripe_connect] = params[:id]
      # redirect_to [:connect_with_stripe, @charity]
      respond_to do |format|
      format.html {redirect_to @charity}
      
      format.json { render json: @charity.errors, status: :unprocessable_entity }
    end
    else
      render 'new'
    end
  end

  def connect_with_stripe
    session[:charity_id_for_stripe_connect] = params[:id]
  end

  def stripe_connected
    @charity = load_my_charity
  end

  def my
    @charity
  end

  def suspend
    @charity = load_my_charity
    @charity.suspend!
  end

  def update
    @charity = load_charity
    raise ActiveRecord::RecordNotFound unless current_user.admin? or current_user.id == @charity.user_id
    @charity.update_attributes(charity_params)
    respond_to do |format|
      format.html {redirect_to @charity}
      
      format.json {respond_with_bip(@charity)}
    end  
  end

  def destroy
    if current_user.admin?
      @charity = Charity.find(params[:id])
      @charity.destroy

    end
    redirect_to charities_path
  end

  def merge_charity
    if current_user.admin?
      @charity = Charity.find(params[:id])
      @charity.merge_charity(params[:in_active_charity_id])
    end
    redirect_to  charities_path,:notice=>"Charity #{@charity.name} was successfully merged"
  end

  private

  def charity_params
    params.require(:charity).permit(:name, :tax_id, :website, :description, :logo, :email, :contact)
  end

  def load_my_charity
    current_user.charities.where(id: params[:id]).first
  end

  def load_charity
    Charity.find(params[:id])
  end
end