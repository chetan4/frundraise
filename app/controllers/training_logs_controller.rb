class TrainingLogsController < ApplicationController
  def new
  end

  def create 
    authenticate_user!
    # training_logs =[]

    fundraiser = current_user.fundraisers.published.active_event_fundraiser_donation.find(params[:training_log][:fundraiser_id])

    if fundraiser.present?
      training_log =fundraiser.training_logs.new(training_log_params)
      training_log.training_date = Date.strptime(training_log_params[:training_date],"%m/%d/%Y") rescue nil
      training_log.user_id = current_user.id
      if training_log.save
         redirect_to fundraiser_path(fundraiser), notice: 'Training log saved'
      else
        redirect_to fundraiser_path(fundraiser), notice: "The following errors occurred while saving your training log: #{training_log.errors.full_messages.join('. ')}"
      end
    else
      redirect_to fundraiser_path(fundraiser),notice: 'No active fundraisers'
    end

    # if current_user.fundraisers.published.present?
    #   current_user.fundraisers.published.active_event_fundraiser_donation.each_with_index do |fundraiser,index|
    #     training_logs[index] = []
    #     training_logs[index][0] = fundraiser.training_logs.new(training_log_params)
    #     training_logs[index][0].training_date = Date.strptime(training_log_params[:training_date],"%m/%d/%Y") rescue nil
    #     training_logs[index][0].user_id = current_user.id
    #     training_logs[index][1] = training_logs[index][0].save 
    #   end
    #   if training_logs[0][1]
    #     redirect_to dashboard_path, notice: 'Training log saved'
    #   else

    #     redirect_to dashboard_path, notice: "The following errors occurred while saving your training log: #{training_logs[0][0].errors.full_messages.join('. ')}"
    #   end
    # else  
    #    redirect_to dashboard_path,notice: 'No active fundraisers'
    # end  
  end
  
  def destroy
    authenticate_user!
    @training_log = TrainingLog.find(params[:id])
    @fundraiser = @training_log.fundraiser
    if current_user.admin?
      @training_log.destroy
    else
      @training_log.destroy
      redirect_to fundraiser_path(@fundraiser),:alert=>"Unauthorized"
    end  
    redirect_to fundraiser_path(@fundraiser),:notice=>"Training Log successfully deleted"   
  end

  private

  def training_log_params
    params.require(:training_log).permit(:workout_name,:miles,:training_date,:fundraiser_id)
  end
end