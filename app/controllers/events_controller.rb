require 'uri'

class EventsController < ApplicationController
  before_filter :redirect_if_already_have_account,only: [:new]

  def index
    @events = Event.all
    search_query = URI.unescape(params[:query]).gsub('+', ' ') if params[:query]

    events = Event.arel_table
    
    # Doesn't work. Change to full text search.
    @events = @events.where(events[:name].matches("%#{search_query}%")).where("happens_on >= ?",Date.today) if search_query

    respond_to do |format|
      format.json{ render :json => @events.to_json }
    end
  end

  def show
    @event = Event.find(params[:id])
    fundraiser_ids = @event.fundraiser_ids
    @miles_trained = TrainingLog.where(fundraiser_id: fundraiser_ids).sum(&:miles)
    @amount_raised = Donation.where(fundraiser_id: fundraiser_ids).non_pledge_donations.map{|d| d.predict_total_donation}.sum
    @charity_partners = @event.charities.order(created_at: :desc).sort_by {|x| x.raised_for_event(@event)}.reverse.each_slice(3)
  end

  def new    
    @event = Event.new
    @event.build_charity_request
  end

  def create
    @event = Event.new(event_params)
    @event.charity_request = CharityRequest.new(charity_request_params)
    charity_request = @event.charity_request

    #event coordinator
    charity_request.user = user_signed_in? ? current_user : User.new(user_params)

    if params[:user].present? && current_user && params[:user][:phone_number].present?
      charity_request.user.phone_number = params[:user][:phone_number]
    end
    @event.valid?
    is_user_valid = charity_request.user.valid?
    charity_request.user.validate_phone_number

    if is_user_valid && @event.save
      charity_request.user.save
      sign_in charity_request.user unless user_signed_in?
      redirect_to confirmation_events_path(event_id: @event.id), notice: "Event registered successfully. We will update your event page shortly
with your Charity Partners. Please contact us at <a href='mailto:admin@frundraise.com' target='_blank'>admin@frundraise.com</a> if you have further questions."
    else
      if @event.errors.has_key? :name
        @event.errors[:name].shift
        @event.errors[:name] = "Event name can't be blank" 
      end
      render :new
    end
  end

  def confirmation
    @event = Event.find_by_id(params[:event_id])
  end

  private

  def event_params
    params.require(:event).permit(:name, :happens_on)
  end

  def user_params
    params.require(:user).permit(:first_name,:last_name,:phone_number,:email,:password,:password_confirmation)
  end

  def charity_request_params
    params.require(:event).require(:charity_request_attributes).permit(:email, :phone_number, :name, :charities)
  end

  def redirect_if_already_have_account
    if params[:login].present?
      session[:user_return_to] = new_event_path
      return redirect_to new_user_session_path
    end
  end
end