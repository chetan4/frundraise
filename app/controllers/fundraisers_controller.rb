class FundraisersController < ApplicationController
  
  before_filter :authenticate_user!, only: [:new, :index]
  layout 'dashboard', only: [ :index] 
  def index
    if current_user.admin?
      @fundraisers = Fundraiser.all
    elsif current_user.charity_admin?
      charity_ids = current_user.charities.pluck(:id)
      @fundraisers = Fundraiser.where("charity_id in (?)",charity_ids).published
    end
    # @support_fundraisers = Fundraiser.where("id in (?)",current_user.donations.pluck(:fundraiser_id).uniq)
    @my_fundraisers = current_user.fundraisers

  end

  def new
    @fundraiser = current_user.fundraisers.new
    @fundraiser.build_charity
    @fundraiser.build_event
    @charity = Charity.find(params[:charity_id]) if params[:charity_id].present?
    @event = Event.find(params[:event_id]) if params[:event_id].present?
  end

  def create
    
    @fundraiser = current_user.fundraisers.new(fundraiser_params)
    @fundraiser.charity = Charity.new(charity_params) if @fundraiser.charity_id.blank?
    @fundraiser.event = Event.new(event_params) if @fundraiser.event_id.blank?
    if @fundraiser.save
      respond_to do |format|
        format.html { redirect_to @fundraiser }
      end
    else
      if params[:fundraiser][:from_event_page].present?
        @event = Event.find_by_id(params[:fundraiser][:event_id])
        @charity = Charity.find_by_id(params[:fundraiser][:charity_id])
      end

      respond_to do |format|
        format.html { render 'new' }
      end
    end
  end

  def show
    @fundraiser = Fundraiser.find(params[:id])
    page = params[:page] || 1
    raise ActiveRecord::RecordNotFound if  @fundraiser.draft? and current_user and current_user != @fundraiser.user and  !current_user.admin?
    @training_logs = @fundraiser.training_logs.order("created_at desc").page(page).per(10)
    # @fundraiser.story = 'Write a short story here.' if @fundraiser.story.blank?
    @can_edit = user_signed_in? && current_user.id == @fundraiser.user_id
    flash[:notice] = "This fundraiser is completed." if @fundraiser.completed?
  end

  def update
    @fundraiser = current_user.fundraisers.where(id: params[:id]).first
    @fundraiser.update_attributes(fundraiser_params)
    respond_to do |format|
      format.html { redirect_to @fundraiser }
      format.json {respond_with_bip(@fundraiser)}
    end
  end

  def publish
    @fundraiser = current_user.fundraisers.where(id: params[:id]).first!
    if @fundraiser.charity.connected_with_stripe?
      @fundraiser.update_attribute :status, 'published'
      redirect_to @fundraiser, notice: 'Your fundraiser is now live!'
    else
      redirect_to @fundraiser, error: "We can't publish your fundraiser yet. Your chosen charity has not completed the registration process yet."
    end
  end

  private

  def fundraiser_params
    params.require(:fundraiser).permit(:name, :goal, :weekly_mileage, :image, :description, :charity_id, :event_id, :story, :user_id)
  end

  def charity_params
    params.require(:fundraiser).require(:charity_attributes).permit(:name, :contact, :phone, :website, :email, :other, :description)
  end

  def event_params
    
    event_attributes = params.require(:fundraiser).require(:event_attributes).permit(:name, :happens_on)
    event_attributes[:happens_on] =  Date.parse(event_attributes[:happens_on]) rescue nil
    event_attributes

  end
end