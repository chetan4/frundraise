class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  before_filter :authenticate_user!, only: [:stripe_connect]
  # def all
    # user = User.from_omniauth()
    # if user.persisted?
    #   flash.notice = "Signed in!"
    #   sign_in_and_redirect user
    # else
    #   session["devise.user_attributes"] = user.attributes
    #   redirect_to new_user_registration_url
    # end
  # end

  def stripe_connect
    auth = request.env["omniauth.auth"]
    charity.save_stripe_auth(auth)
    if charity.reload.connected_with_stripe?
      redirect_to [:stripe_connected, charity]
    else
      redirect_to [:connect_with_stripe, charity], notice: "Something went wrong while trying to connect your charity with Stripe. Please try again."
    end
  end

  private

  def charity
    if session[:charity_id_for_stripe_connect].present?
      current_user.charities.where(id: session[:charity_id_for_stripe_connect]).first
    else
      current_user.charities.order(created_at: :desc).last    
    end
  end

  # alias_method :twitter, :all
  # alias_method :stripe_connect, :all
end