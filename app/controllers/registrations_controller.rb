class RegistrationsController < Devise::RegistrationsController
  def after_sign_up_path_for(resource)
    if session[:user_return_to].present?
      session[:user_return_to]
    else
      root_path
    end
  end
end