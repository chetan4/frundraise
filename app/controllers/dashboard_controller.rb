class DashboardController < ApplicationController
  layout 'dashboard'
  before_filter :authenticate_user!
  
  def index

    @supported_fundraisers = current_user.supported_fundraisers
    @my_charities = current_user.charities
    @my_fundraisers = current_user.fundraisers

    relevant_fundraiser_ids = []
    relevant_fundraiser_ids += @supported_fundraisers.map(&:id)
    relevant_fundraiser_ids += @my_fundraisers.map(&:id)

    ids_for_my_charities = @my_charities.map(&:id)
    ids_for_fundraisers_in_my_charities = Fundraiser.where(charity_id: ids_for_my_charities).ids
    relevant_fundraiser_ids += ids_for_fundraisers_in_my_charities
    
    relevant_fundraiser_ids.uniq!
    @activities = Activity.order(created_at: :desc).where("(meta_data-> 'user_id')::int IN (?) OR (meta_data -> 'fundraiser_id')::int IN (?)", current_user.id, relevant_fundraiser_ids).limit(20)
    # Result in [["donation_id"=>sum value"]]
    # top_donations = Donation.where(fundraiser_id: (@my_fundraisers.map(&:id) + ids_for_fundraisers_in_my_charities)).limit(10)
    donations = Donation.where("fundraiser_id in (?) and error_message is ?", (@my_fundraisers.map(&:id) + ids_for_fundraisers_in_my_charities),nil).group_by(&:user_id)
    top_donations = {}
    donations.each  {|user_id,donations| top_donations[user_id]= donations.sum{|x|x.predict_total_donation} }
    @top_donations = top_donations.sort_by {|_key, value| value}.reverse


    #@top_donations = Donation.where(fundraiser_id: (@my_fundraisers.map(&:id) + ids_for_fundraisers_in_my_charities)).group("user_id").sum("fixed_amount").sort_by {|_key, value| value}.reverse
   
  end
end
