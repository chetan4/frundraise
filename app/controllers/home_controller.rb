class HomeController < ApplicationController
  
  def index
  	# Code commented bcoz of page loading issue
    # @events = Event.order(created_at: :desc).includes(:fundraisers => :donations).where("happens_on >= ? ",Date.today).sort_by{|e| e.donations_total}.reverse[0...5]
    # @donations = Donation.where('created_at >= ?', 6.months.ago)
    # @charities = Charity.order(created_at: :desc).sort_by {|x| x.raised}.reverse[0...3]
    # Temporary code to reduce pge load time with hard code values
    @ids = [94, 35, 56]
    # @charities = Charity.where("id IN (?)", @ids)
    @charities = Charity.where(id: @ids).order("position(id::text in '#{@ids.join(',')}')")
    if @charities.blank?
      @names = ["Ronald McDonald House Charities of Central Ohio", "Tri 4 Schools", "Bicycles For Humanity"]
      @charity_names = @names.map(&:downcase)
      @charities = Charity.where("lower(name) IN (?)", @charity_names)
    end
  end
    # @events = Event.order(created_at: :desc).where("happens_on >= ? ",Date.today).sort_by{|e| e.donations_total}.reverse[0...5]
    # @charities = Charity.order(created_at: :desc).sort_by {|x| x.raised}.reverse[0...3]
    # @charities = Charity.where('created_at >= ?', 3.months.ago).sort_by {|x| x.raised}.reverse[0...3]
end