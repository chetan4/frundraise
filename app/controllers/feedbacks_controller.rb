class FeedbacksController < ApplicationController
  def create
    @feedback = Feedback.create(feedback_params)
    redirect_to request.referer, notice: 'Thank you for your feedback.'
  end

  private

  def feedback_params
    params.require(:feedback).permit(:comment)
  end
end