class DonationsController < ApplicationController
  before_filter :authenticate_user!, only: [:index, :show]
  before_filter :fundraiser,only: [:new,:create]
  layout 'dashboard', only: [:index, :show]
  
  def index
    @my_donations = current_user.donations.where(error_message: nil)
    @fundraiser_donations = Donation.where("fundraiser_id in (?) and error_message is ?",current_user.fundraiser_ids,nil).order("created_at desc")
    charity_fundraiser_ids  = Fundraiser.where("charity_id in (?)",current_user.charity_ids).order("created_at desc").select("id").pluck("id")
    @charity_donations = Donation.where("fundraiser_id in (?) and error_message is ?",charity_fundraiser_ids,nil).order("created_at desc")
  end

  def new
    @fundraiser = fundraiser
    @donation = @fundraiser.donations.new

    if user_signed_in?
      @donation.user = current_user
    else
      @donation.build_user
    end
    if @fundraiser.completed?
      redirect_to info_charities_path
    end

  end

  def create
    @fundraiser = fundraiser
    @donation = @fundraiser.donations.new(donation_params)
    @donation.user = user_signed_in? ? current_user : User.new(user_params)
    @donation.user.valid?
      
    
    # raise
    
    if @donation.user.valid? 
      if  @donation.save

        sign_in @donation.user if !user_signed_in?  && @donation.user.valid?
        if @donation.error_message.blank?
          redirect_to dashboard_path, notice: 'Donation has been made.'
        else
          error_message = @donation.error_message
          
          redirect_to new_fundraiser_donation_path(@fundraiser),:alert=>"Error: #{error_message}"  
        end  
      else
        error_message = @donation.errors.full_messages.join(",").gsub("Fixed amount","")
        
        redirect_to new_fundraiser_donation_path(@fundraiser,@donation),:alert=>"Error: #{error_message}"
        # render :action=>:new
        # raise '@donation.stripe_token not present'
      end
    else  
      
      error_message = @donation.user.errors.full_messages.join(",")
      render :action=>:new,:notice=>"Error: #{error_message}"
      # redirect_to new_fundraiser_donation_path(@fundraiser,@donation),:notice=>"Error: #{error_message}"
    end  
  end

  def show
    @donation = current_user.donations.where(id: params[:id]).first
  end

  private

  def donation_params
    params.require(:donation).permit(:fixed_amount, :per_mile_amount, :max_per_mile_amount, :stripe_token)
  end

  def user_params
    params.require(:donation).require(:user).permit(:email, :first_name, :last_name, :password, :password_confirmation)
  end

  def fundraiser
    begin
      @fundraiser ||= Fundraiser.published.find(params[:fundraiser_id]) 
    rescue
      redirect_to root_url ,:notice=>"Fundraiser is not found" 
    end  
  end
end