class PledgeWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable
  recurrence { daily }
    # recurrence {hourly.minute_of_hour(0, 1,2,3,5, 30, 45) }
   sidekiq_options :queue => :pledge_queue, :retry => 2, :backtrace => true
   
  # TODO SEGREGATE MORE LIKE:
  # INDIVIDUAL DONATION CAN PROCESS IN SIDEKIQ
  def perform
    puts "started"
    
    Fundraiser.active_event_fundraiser_donation.each do |fundraiser|
      fundraiser.process_pledge_donation
    end  
  end

end