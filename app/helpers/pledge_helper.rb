module PledgeHelper
  def estimate_pledge_donation_dates(fundraiser,donation_date= Date.today)
    Donation.estimate_pledge_dates(fundraiser,donation_date)
  end
end