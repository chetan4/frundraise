module ApplicationHelper
  def render_activity(activity)
    activity.meta_data
    case activity.activity_type
    when 'new_training_log'
      render_new_training_log_activity(activity)
    when 'new_donation'
      render_new_donation_activity(activity)
     when 'new_plegde_donation'
      render_new_pledge_donation_activity(activity)
    when 'new_pledge'
      render_new_pledge_activity(activity)  
    when 'new_user'
      render_new_user_activity(activity)
    end
  end

  def render_new_training_log_activity(activity)
    user = User.find(activity.user_id.to_i)
    user_name = (current_user.id == user.id) ? 'You' : user.name.capitalize
    fundraiser = Fundraiser.find(activity.fundraiser_id.to_i)

    logger.debug "ApplicationHelper.render_new_training_log_activity user: #{user}, user_name: #{user_name}, fundraiser: #{fundraiser} "
    
    content_tag :div, class: 'activity' do
      if fundraiser.image.url.present?          
        image = image_tag( fundraiser.image.url(:thumb), class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px;width:40px;height:40px' ) 
      else
        image = image_tag( 'fundraiser-logo.png', class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px; width:40px;height:40px' ) 
      end  
      

      content = content_tag :p do
        if activity.training_date.present?
          "#{user_name} trained #{activity.miles_trained} miles for #{link_to fundraiser.name, fundraiser} on #{activity.training_date.to_date.strftime("%B %e")}.".html_safe
        else
          # "#{user_name} trained #{activity.miles_trained} miles for #{link_to fundraiser.name, fundraiser} - #{timestamp(activity)}.".html_safe
          "#{user_name} trained #{activity.miles_trained} miles for #{link_to fundraiser.name, fundraiser} - #{timestamp(activity)}.".html_safe
        end  
      end

      clear = content_tag :div, '', class: 'clearfix'

      [image, content, clear].compact.join('').html_safe
    end
  end

  def render_new_donation_activity(activity)
    # binding.pry
    
      user = User.find(activity.user_id.to_i)  rescue nil

      user_name = (current_user.id == user.id) ? 'You' : user.name.capitalize  if user
      fundraiser = Fundraiser.includes(:charity).where(id: activity.fundraiser_id.to_i).first

      logger.debug "ApplicationHelper.render_new_donation_activity user: #{user}, user_name: #{user_name}, fundraiser: #{fundraiser} "
      
      if current_user.fundraiser_belongs_to_me? fundraiser
        content = content_tag :p do
          "#{user_name} donated #{number_to_currency(activity.fixed_amount)} to #{link_to fundraiser.name, fundraiser} - #{timestamp(activity)}.".html_safe
        end
        if fundraiser.image.url.present?          
          image = image_tag( fundraiser.image.url(:thumb), class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px;width: 40px;height: 40px' ) 
        else
          image = image_tag( 'fundraiser-logo.png', class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px; width: 40px;height: 40px' ) 
        end  
        clear = content_tag :div, '', class: 'clearfix'

        content_tag :div, class: 'acitivity' do
          [image, content, clear].compact.join('').html_safe
        end
      end
    
  end
  def render_new_pledge_donation_activity(activity)
    
      user = User.find(activity.user_id.to_i)  rescue nil
      logger.error "=====================#{activity.inspect}"
      user_name = (current_user.id == user.id) ? 'You' : user.name.capitalize  if user
      fundraiser = Fundraiser.includes(:charity).where(id: activity.fundraiser_id.to_i).first
      logger.error "ApplicationHelper.render_new_donation_activity user: #{user}, user_name: #{user_name}, fundraiser: #{fundraiser} "
      max_per_mile_amount = "(#{ number_to_currency(activity.max_per_mile_amount)} max. per month)" if activity.max_per_mile_amount.present?
      # if current_user.fundraiser_belongs_to_me? fundraiser
      logger.error "========================== #{fundraiser.inspect}" 
        content = content_tag :p do
           # pledged $0.10 per mile ($50.00 max. per month) to <fundraiser name>.
          
          "#{user_name} pledged #{number_to_currency(activity.per_mile_amount)} #{max_per_mile_amount} to #{link_to fundraiser.name, fundraiser} - #{timestamp(activity)}.".html_safe
        end
        if fundraiser.image.url.present?          
          image = image_tag( fundraiser.image.url(:thumb), class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px;width:40px;height:40px' ) 
        else
          image = image_tag( 'fundraiser-logo.png', class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px; width:40px;height:40px' ) 
        end  
        clear = content_tag :div, '', class: 'clearfix'

        content_tag :div, class: 'acitivity' do
          [image, content, clear].compact.join('').html_safe
        end
      # end
    
  end
  def render_new_pledge_activity(activity)
    user = User.find(activity.user_id.to_i)  rescue nil

    user_name = (current_user.id == user.id) ? 'You' : user.name.capitalize  if user
    fundraiser = Fundraiser.includes(:charity).where(id: activity.fundraiser_id.to_i).first

    logger.debug "ApplicationHelper.render_new_donation_activity user: #{user}, user_name: #{user_name}, fundraiser: #{fundraiser} "
    
    if current_user.fundraiser_belongs_to_me? fundraiser

      content = content_tag :p do
        "#{user_name} donated a pledge amount #{number_to_currency(activity.fixed_amount)} to #{link_to fundraiser.name, fundraiser} - #{timestamp(activity)}.".html_safe
      end
      if fundraiser.image.url.present?          
        image = image_tag( fundraiser.image.url(:thumb), class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px;width:40px;height:40px' ) 
      else
        image = image_tag( 'fundraiser-logo.png', class: 'pull-left', style: 'margin-right: 10px; margin-bottom: 10px; width:40px;height:40px' ) 
      end  
      clear = content_tag :div, '', class: 'clearfix'

      content_tag :div, class: 'acitivity' do
        [image, content, clear].compact.join('').html_safe
      end
    end
  end

  def render_new_user_activity(activity)
    user = User.find(activity.user_id.to_i)
    user_name = (current_user.id == user.id) ? 'You' : user.name.capitalize

    content_tag :div, class: 'acitivity' do
      content_tag :p do
        "#{user_name} joined Frundraise #{timestamp(activity)}.".html_safe
      end
    end
  end

  def timestamp(activity)
    content_tag:span, class: 'timestamp', style: 'color: gray' do
     time_ago_in_words(activity.training_date || activity.created_at) + ' ago'
    end.html_safe
  end

  def active_class(class_name)
     classes = {
      "fundraisers.new"=>"new_fundraiser",
      "pages.how-it-works"=>"how-it-work",
      "fundraisers"=>"fundraiser",
      "charities"=>"charity",
      "donations"=>"donation",
      "dashboard"=>"dashboard",
      "events"=>"events"
    }
    
    "active" if class_name == (classes[controller.controller_name + '.' + controller.action_name] || classes[controller.controller_name] || '')
  end
end
