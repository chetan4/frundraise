module EventsHelper
	def user_has_phone_number?
		!user_signed_in? || (user_signed_in? && current_user.phone_number.nil?)
	end
end