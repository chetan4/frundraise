module CharitiesHelper
	def show_inline_story_heading(v)
    if v.blank? 
      link_to "Write a short story here.","#"
    else
      simple_format(v)
    end  
  end
end