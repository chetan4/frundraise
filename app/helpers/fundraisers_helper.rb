module FundraisersHelper
  def show_inline_story_heading(v)
    if v.blank? 
      link_to "Write a short story here.","#"
    else
      simple_format(v)
    end  
  end

  def can_edit_training_log?(training_log)
    training_log.created_at+1.hour >= Time.now && current_user == training_log.fundraiser.user 
  end
end