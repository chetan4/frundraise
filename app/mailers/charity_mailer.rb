class CharityMailer < ActionMailer::Base
  default from: "admin@frundraise.com", bcc: 'admin@frundraise.com'

  def new_charity_invite(body, recipient, athlete)
    @body = body
    subject = "#{athlete} wants to raise money for you on Frundraise"
    mail(to: recipient, subject: subject)
  end

  def donation_receipt(donation)
    @donation = donation
    subject = "Frundraiser donation receipt"
    mail(to: @donation.user.email,subject: subject)
  end

  def send_feedback(comment)
    @comment = comment
    mail(to: 'admin@frundraise.com',subject: "Feedback" )
  end
  # Send Sign up info to jinesh for marketing purpose
  def send_sign_up(user)
    @user =user
    mail(to: 'jparekh@idyllic-software.com',subject: "Frundraise Signup info" )
  end

  def donation_exception(donation,error)
    @donation = donation
    @error = error
    mail(to: 'mayur@idyllic-software.com',subject: "Frundraise Donation Exception" )
  end
end
