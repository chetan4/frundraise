class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable

  include UserValidator
  has_many :fundraisers
  has_many :supported_fundraisers, through: :donations, source: :fundraiser

  has_many :charities
  has_many :donations

  has_many :charity_requests

  after_create :create_new_user_activity
  after_create :send_sign_up

  def create_new_user_activity
    Activity.create( activity_type: 'new_user', 
      meta_data: { 
        user_id: self.id
      })
  end

  def send_sign_up
    CharityMailer.send_sign_up(self).deliver
  end

  def name
    name = []
    name << first_name if first_name.present?
    name << last_name if last_name.present?
    if name.empty?
      name = ''
    else
      name = name.join(' ')
    end
    
    name.present? ? name : email.split('@').first.gsub(/[.\-_]/, ' ').titlecase rescue ""
  end
  
  def display_first_name
    if  first_name.present?
      first_name
    elsif last_name.present?
      last_name
    else
      email.split('@').first.gsub(/[.\-_]/, ' ').titlecase
    end
    
  end
  def self.from_omniauth(auth)
    case auth.slice(:provider)
    when 'stripe_connect'
      from_stripe_connect(auth)
    when 'twitter'
      from_twitter(auth)
    end
  end

  def self.from_twitter(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.username = auth.info.nickname
    end
  end

  def self.from_stripe_connect(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.username = auth.info.nickname
    end
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def password_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

  def admin?
    role == 'admin'
  end

  # IF any charity is present he is owner of charity
  def charity_admin?
    self.charities.present?
  end

  # started any fundraiser  
  def fundraiser?
    self.fundraisers.present?
  end
  def supporter?
    !admin? && !charity_admin? && !fundraiser?
  end
  def full_name
    [self.first_name, self.last_name].compact.join(' ')
  end

  def total_reportable_donations_for_day(day)
    donations = Donation.where(fundraiser_id: relevant_fundraisers).non_pledge_donations.where('created_at < ?', day).to_a
    donations.sum { |d| d.total_to_date(day) }
  end

  def reportable_donations(number_of_days)
    new_values = []
    (1..number_of_days).map{ |x| new_values << total_reportable_donations_for_day((number_of_days-x).days.ago) }
    new_values
  end

  def total_reportable_miles_for_day(day)
    training_miles = TrainingLog.where(fundraiser_id: relevant_fundraisers).where('training_date <= ?', day).to_a
    training_miles.sum(&:miles)
  end

  def reportable_miles(number_of_days)
    new_values = []
    (1..number_of_days).map{ |x| new_values << total_reportable_miles_for_day((number_of_days-x).days.ago) }
    new_values
    
    # groups = training_miles.group_by{ |training_mile| training_mile.created_at.beginning_of_day }
    # new_values = []
    # groups.sort.each do |day, daily_training_miles|
    #   daily_amount = daily_training_miles.sum(&:miles).to_i
    #   new_values << daily_amount ? daily_amount : 0
    # end
    # new_values
  end

  def relevant_fundraisers
    ids = []
    ids += self.fundraiser_ids
    ids += Fundraiser.where("charity_id in (?)", self.charity_ids).pluck(:id)
    ids.flatten.compact.uniq  
  end

  def fundraiser_belongs_to_me? fundraiser
    self.id == fundraiser.user_id or self.id == fundraiser.charity.user_id
  end

  def predict_total_donations
    self.donations.non_pledge_donations.map {|d| d.predict_total_donation}.sum
  end
end
