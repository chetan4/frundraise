class Donation < ActiveRecord::Base
  extend ActionView::Helpers::NumberHelper
  include Pledgeable
  MINIMUM_DONATION_AMOUNT =  0.50
  FEE_PERCENTAGE = 5.0
  belongs_to :user
  belongs_to :fundraiser
  # Tracking pledge donations (parent_id is initial pledge donation)
  belongs_to :parent, :class_name => "Donation"
  has_many :childrens, :foreign_key => "parent_id",:class_name=>"Donation"

  attr_accessor :terms
  attr_accessor :stripe_card_token

  validates :fixed_amount, :numericality => {greater_than_or_equal_to: MINIMUM_DONATION_AMOUNT,:message=>"The minimum donation is #{number_to_currency(MINIMUM_DONATION_AMOUNT)}" },:allow_blank=>true
  validates_numericality_of :max_per_mile_amount, greater_than_or_equal_to: 0.5, message: "The minimum amount is $0.50 for the maximum per month on your pledge.", if: Proc.new {|obj| obj.max_per_mile_amount.present?}

  validate :create_stripe_customer
  after_create :charge_fixed_amount
  after_create :update_total!

  
  # accepts_nested_attributes_for :user
  validate :donation_amount
  scope :pledge_donations,lambda{where("pledge_date is not null")}
  scope :non_pledge_donations,lambda{where("pledge_date is null AND error_message is null")}
  scope :initial_pledges,lambda{where("per_mile_amount is not null")}

  def donation_amount
     if fixed_amount.blank? && per_mile_amount.blank?
        self.errors.add(:base,"Please enter per mile donation or fixed donation")
     end

  end 

  def create_activity
    Activity.create( activity_type: 'new_donation', 
      meta_data: { 
        fundraiser_id: self.fundraiser_id, 
        fixed_amount: self.fixed_amount,
        per_mile_amount: self.per_mile_amount,
        user_id: self.user_id
      })
  end

  def create_pledge_activity
    Activity.create( activity_type: 'new_plegde', 
      meta_data: { 
        fundraiser_id: self.fundraiser_id, 
        fixed_amount: self.fixed_amount,
        max_per_mile_amount: self.max_per_mile_amount,
        per_mile_amount: self.per_mile_amount,
        user_id: self.user_id,
        pledge_date: self.pledge_date
      })
   
  end

  def create_new_pledge_donation_activity
    Activity.create( activity_type: 'new_plegde_donation', 
      meta_data: { 
        fundraiser_id: self.fundraiser_id, 
        max_per_mile_amount: self.max_per_mile_amount,
        per_mile_amount: self.per_mile_amount,
        user_id: self.user_id
      })

  end

  def create_stripe_customer
    logger.debug "Donation#create_stripe_customer donation.id: #{self.id}"

    if self.stripe_token.present? 
      if self.stripe_customer_id.blank?
        logger.error "stripe_token: #{self.stripe_token}"
        begin
          customer = Stripe::Customer.create(card: self.stripe_token, description: self.user.email,email: self.user.email)
          logger.error "Stripe::Customer.create response: #{customer.inspect} #{customer}"
          self.stripe_customer_id = customer.id
        rescue => e
          errors.add(:stripe_customer_id,e.message)
        end  
      end  
    elsif self.parent.blank?
      errors.add(:stripe_customer_id,"Something went wrong,please insert card information again.")   
    end
  end

  def charge_fixed_amount
    if self.fixed_amount.present?
      begin
        logger.debug "Donation#charge_fixed_amount donation.id: #{self.id}"
        stripe_access_token = self.fundraiser.charity.stripe_access_token
        logger.debug "self.fundraiser.charity.stripe_access_token: #{stripe_access_token}"
        token = Stripe::Token.create( { customer: self.stripe_customer_id }, stripe_access_token )
        8.times { Rails.logger.info "******************************************" }
        Rails.logger.info "-------------- #{fundraiser.fee_percentage} ----------" 
        8.times { Rails.logger.info "******************************************" }
        charge = Stripe::Charge.create({ 
          amount: (self.fixed_amount * 100).to_i, 
          currency: "usd", 
          card: token.id, 
          description: "Fixed donation", 
          application_fee:  (self.fixed_amount * fundraiser.fee_percentage).to_i }, 
          stripe_access_token )

        logger.debug "Stripe::Charge.create response: #{charge}"
        # if self.pledge_date.present? ? 
          # created_pledge_activity
          create_activity
        # else
        #   create_activity
        # end
        send_donation_receipt
        charge
      rescue => e
        self.error_message = e.message
        self.save
        CharityMailer.donation_exception(self,e).deliver
      end
    elsif self.per_mile_amount.present?
      create_new_pledge_donation_activity          
    end  
  end

  def send_donation_receipt
    CharityMailer.donation_receipt(self).deliver
  end

  # Update total fundraiser amount if it's fixed donation
  # Predict pledge donation
  def update_total!
    if self.error_message.blank?
      amount = 0
      amount += self.fixed_amount.to_f if self.fixed_amount.present? && self.pledge_date.blank?
      # amount += (self.fundraiser.training_logs.sum(&:miles) * self.per_mile_amount) if self.per_mile_amount
      self.update_attribute :total, amount
      self.fundraiser.total_donations =   self.fundraiser.total_donations + self.total
      self.fundraiser.save
    else
      error_message = self.error_message
    end
  end

 
  def get_total_pledge_donation
    self.predicate_pledge_donation.to_f
  end

  def is_initial_pledge?
    self.per_mile_amount.present?
  end
  def predict_total_donation
    self.fixed_amount.to_f + self.predicate_pledge_donation.to_f
  end

  def total_to_date(day)
    amount = 0
    amount += self.fixed_amount.to_f
    # 
    amount += self.predicate_pledge_donation(day).to_f
    # amount += self.fundraiser.training_logs.where("created_at < ?", day).sum(&:miles) * self.per_mile_amount if self.per_mile_amount
    amount
  end

  rails_admin do 
    include_all_fields
    field :created_at
  end
end
