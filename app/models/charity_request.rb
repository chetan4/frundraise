class CharityRequest < ActiveRecord::Base
	validates_presence_of :charities, :message => "Please enter one or more Charity Partners that you want to connect to the event."
	belongs_to :event
	belongs_to :user
end
