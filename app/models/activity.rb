class Activity < ActiveRecord::Base
  store_accessor :meta_data, :fundraiser_id, :miles_trained, 
    :user_id, :fixed_amount, :per_mile_amount,:training_date,:pledge_date,:max_per_mile_amount
end
