class Fundraiser < ActiveRecord::Base
  mount_uploader :image, ProfileUploader

  has_many :donations,:dependent=>:destroy
  has_many :training_logs,:dependent=>:destroy
  belongs_to :user
  belongs_to :charity
  belongs_to :event
  validates_presence_of :name

  accepts_nested_attributes_for :charity
  accepts_nested_attributes_for :event

  delegate :fee_percentage,to: :charity
  after_destroy :delete_all_activities
  PER_MILE_DONATION_SUGGESTIONS = (1..10).map do |n| 
    n = (n/10.0).round(2)
    OpenStruct.new(amount: n, label: "$#{ sprintf('%.2f', n)}")
  end

  scope :published, -> { where(status: 'published') }
  scope :active_event_fundraisers, -> { joins(:event).where("events.happens_on >= ?",Date.today) }
  scope :active_event_fundraiser_donation, -> { joins(:event).where("events.happens_on >= ?",Date.today-10.days) }

  def update_donation_totals
    amount = 0
    self.donations.each do |donation|
      donation.update_total!
      amount += donation.total
    end
    self.update_attribute :total_donations, amount
  end

  def total_miles
    self.training_logs.sum(&:miles)
  end

  def status?
    self.status
  end

  def published?
    self.status == 'published'
  end

  def completed?
    self.update_attributes(:status=>"completed") if self.event.happens_on < Date.today-10.days
    self.status == "completed"
  end

  def draft?
    self.status == 'draft'
  end
  
  def raised
    predict_total_donations
  end

  def predict_pledge_donation
    sum = 0
    self.donations.initial_pledges.map{|d| sum = sum + d.predicate_pledge_donation}
    sum
  end

  def predict_total_donations
    self.donations.non_pledge_donations.map {|d| d.predict_total_donation}.sum
  end

  def process_pledge_donation
    self.donations.initial_pledges.each do |donation|
      begin
        donation.make_pledge_donations
      rescue => e
        CharityMailer.donation_exception(donation,e).deliver
      end  
    end
  end

  def delete_all_activities
    Activity.where("(meta_data -> 'fundraiser_id')::int in (?)",id).delete_all
  end

  rails_admin do     
    include_all_fields
    field :created_at
  end
end
