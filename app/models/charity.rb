class Charity < ActiveRecord::Base
  include Tokenable
  mount_uploader :logo, LogoUploader

  has_many :fundraisers,:dependent=>:destroy
  belongs_to :user
  has_many :event_charities
  has_many :events, through: :event_charities

  validates_presence_of :name

  #commenting following validation in order to allow user to have multiple charities
  #validates_uniqueness_of :user_id,:message=>"You are already the owner of one charity on Frundraise. If you would like to create a second charity, you first need to create a second account to use for that charity.",:allow_nil=>true
  validates_format_of :website, :with =>/(http|https):\/\/|[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?/ix,:allow_nil=>true,:allow_blank=>true
  scope :not_connected_with_stripe, -> { where(stripe_access_token: nil).where(stripe_publishable_key: nil).where(stripe_user_id: nil) }
  scope :connected_with_stripe, -> { where.not(stripe_access_token: nil).where.not(stripe_publishable_key: nil).where.not(stripe_user_id: nil) }

  scope :active, -> { where.not(user_id: nil) }
  scope :inactive, -> { where(user_id: nil) }

  before_create :save_token

  def connected_with_stripe?
    stripe_access_token.present? and stripe_user_id.present? and stripe_publishable_key.present?
  end

  def save_token
    return if self.user
    self.token = generate_token
  end

  def save_stripe_auth(auth)
    # logger.debug "Charity#save_stripe_auth"
    # logger.debug "auth = " + auth.inspect

    # logger.debug "stripe_access_token = #{auth["credentials"]["token"]}"
    # logger.debug "stripe_user_id = #{auth["uid"]}"
    # logger.debug "stripe_publishable_key = #{auth["info"]["stripe_publishable_key"]}"

    self.stripe_access_token = auth["credentials"]["token"]
    self.stripe_user_id = auth["uid"]
    self.stripe_publishable_key = auth["info"]["stripe_publishable_key"]

    valid? ? save! : logger.debug("Error occurred while saving: #{errors.full_messages.join('. ')}")
  end

  def as_json(options={})
    super(:methods => [:image_url], :only => [:id, :name, :description, :website, :address, :phone])
  end

  def raised
    Donation.where(fundraiser_id: self.fundraiser_ids).non_pledge_donations.map{|x|x.predict_total_donation}.sum
  end

  def supporters
    Donation.where(fundraiser_id: self.fundraiser_ids).count + self.fundraisers.count
  end

  def raised_for_event(event)
    fundraisers_ids = fundraisers_for_event(event).map(&:id)
    Donation.where(fundraiser_id: fundraisers_ids).non_pledge_donations.map{|x|x.predict_total_donation}.sum
  end

  def supporters_for_event(event)
    fundraisers_ids = fundraisers_for_event(event).map(&:id)
    Donation.where(fundraiser_id: fundraisers_ids).count + fundraisers_ids.count
  end

  def fundraisers_for_event(event)
    self.fundraisers.published.select{|f| f.event_id == event.id}
  end

  def image_url
    self.logo ? self.logo.url(:thumb) : ''
  end

  def active?
    connected_with_stripe?
  end
  
  def website_url

    if self.website.present?
      unless self.website[/\Ahttp:\/\//] || self.website[/\Ahttps:\/\//]
        "http://#{self.website}"
      else
        self.website  
      end
    else
     ""  
    end  
  end

  def merge_charity(in_active_charity_id)
    charity = Charity.find(in_active_charity_id)
    fundraisers = self.fundraisers+charity.fundraisers
    self.fundraisers = fundraisers.flatten.compact.uniq
    if self.save!
      charity.delete #replaced destroy with delete
      true
    else
      false  
    end
  end

  def active_fundraisers
    self.fundraisers.select{|f| f.event.happens_on >= Date.today}.compact
  end

end
