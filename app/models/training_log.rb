class TrainingLog < ActiveRecord::Base
  belongs_to :fundraiser
  validates_presence_of :miles,:training_date
  validates_numericality_of :miles
  validate :validate_training_date

  scope :between_training_date,lambda{|start_date,end_date| where("created_at >= ? and created_at <= ?",start_date.beginning_of_day,end_date.end_of_day)}
  attr_accessor :user_id
  after_create :add_activity
  after_destroy :delete_activity

  # before_destroy :can_deleted?

  def add_activity(user_id=nil)
    Activity.create( activity_type: 'new_training_log', 
          meta_data: { 
            fundraiser_id: self.fundraiser_id, 
            miles_trained: self.miles,
            user_id: user_id || self.fundraiser.user_id,
            training_date: self.training_date,
            training_log_id: self.id
          })
  end  

  def validate_training_date
    errors.add(:training_date,"can not be in future") if self.training_date.present? && self.training_date > Date.today
  end

  def update_fundraiser_totals
    self.fundraiser.update_donation_totals
  end
  def can_deleted?
    self.created_at+1.hour >= Time.now
  end
  def activities
    Activity.where("meta_data -> 'training_log_id' = '#{self.id}'")
  end
  def delete_activity
    self.activities.destroy_all
  end
  rails_admin do   
    include_all_fields
    field :created_at
  end
end
