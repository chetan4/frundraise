class Feedback < ActiveRecord::Base
  validates_presence_of :comment
  after_create :send_feedback
  def send_feedback
    CharityMailer.send_feedback(self.comment).deliver
  end
end
