class Event < ActiveRecord::Base
  has_many :fundraisers

  validates_presence_of :name
  validates_presence_of :happens_on,:message=>"Please set a future date for your event"
  
  validate :happens_on_cannot_be_in_the_past
  has_many :event_charities
  has_many :charities, through: :event_charities
  
  has_one :charity_request, dependent: :destroy
  accepts_nested_attributes_for :charity_request

  scope :active_events,lambda{where("happens_on >= ?",Date.today)}
  def happens_on_cannot_be_in_the_past
    
    errors.add(:happens_on,"Please set a future date for your event") if happens_on.present? &&  happens_on < Date.today  

  end
  
  def miles_trained
    TrainingLog.where(fundraiser_id: fundraiser_ids).sum(&:miles)
  end

  def donations_total
    self.fundraisers.map{|x|x.predict_total_donations}.sum
  end

  def completed?
    self.happens_on > Date.today
  end
end
