module Pledgeable
  extend ActiveSupport::Concern
  
 
  
  # Iterate month wise and get total miles for that period
  # calculate donation amount if it's greater than max_per_mile_amount then set donation max_per_mile_amount if max_per_mile_amount present
  def get_total_miles_for_months(mile_end_date=nil)
    recent_donation = get_recent_donation
    start_date = recent_donation.created_at.to_date
    # Set End date for calculation
    # End date default is today date 
    # if end date is greater than evenet_date+10.days then set event_date+10.days for processing donations
    # return @array of {:total_miles=>total_miles,:start_date=>month_start_date,:end_date=>month_end_date,:amount=>donation_amount}
    event_date = (self.fundraiser.event.happens_on+10.days).to_date
    end_date = mile_end_date || Date.today
    end_date = end_date > event_date ? event_date.to_date : end_date.to_date
    month_start_date,month_end_date = start_date,start_date
    month_miles = []

    begin
      month_end_date = month_end_date+1.month
      # If month end date is greater than end event date 
      month_end_date = month_end_date > event_date ? event_date : month_end_date
      # If user supply end date for calculation restricr calculation for date
       # mainly useful for display in dashboard
      month_end_date = month_end_date > end_date ? end_date : month_end_date if mile_end_date.present?
      # Get Total Miles
      total_miles= self.fundraiser.training_logs.between_training_date(month_start_date,month_end_date).sum("miles").to_f
      # Get  Total donation amount
      donation_amount = self.per_mile_amount.to_f*total_miles.to_f
      # Donation is greater to max amount set donation amount as max amount
      donation_amount =  donation_amount > max_per_mile_amount ? max_per_mile_amount.to_f : donation_amount if max_per_mile_amount.present?  && max_per_mile_amount >=0.0
      month_miles << {:total_miles=>total_miles,:start_date=>month_start_date,:end_date=>month_end_date,:amount=>donation_amount}
      # Update start date after 1 day for next iteration
      month_start_date = month_end_date+1.day
    end while (month_end_date < end_date)  
    month_miles
  end

  def get_recent_donation
    if self.childrens.any?
      self.childrens.order('created_at DESC').first
    else
      self
    end
  end

  def get_total_pledge_amount
    total_donations = 0.0
    get_total_miles_for_months.collect {|x|total_donations = total_donations + x[:amount]}
    total_donations
  end
  
  # Check Month Donation is greater than MINIMUM DONATION
  # IF not then accumulate with next one 
  def make_pledge_donations
    month_miles = get_total_miles_for_months
    stripe_customer_id = self.stripe_customer_id
    donation_amount = 0
    month_miles.each do |month_mile|
      end_date = month_mile[:end_date] 
      donation_amount = donation_amount + month_mile[:amount]
      donation_amount = donation_amount.to_f > max_per_mile_amount ? max_per_mile_amount : donation_amount.to_f if max_per_mile_amount.present? && max_per_mile_amount.to_f > 0

      if end_date <= Date.today
        #  Check donation exist for pledge  date
        # pledge_date >= because some time pledge donation is less than minimum amount and pledge is not proceed
        donation = Donation.where("pledge_date >= ? and  fundraiser_id = ? and user_id = ? and parent_id = ?",end_date.to_date,self.fundraiser_id,user_id,self.id).limit(1).first
        # donation date is blank and donation amount is grrater than equal to min donation amount
        if donation.blank? && donation_amount >= Donation::MINIMUM_DONATION_AMOUNT

          pledge_donation =Donation.new(:fundraiser_id=>self.fundraiser_id,:user_id=>self.user_id,:stripe_customer_id=>self.stripe_customer_id,:fixed_amount=>donation_amount,:pledge_date=>end_date.to_date,:parent_id=>self.id)
          pledge_donation.save
          donation_amount =0  
        end
      end  

    end
  end

  def predicate_pledge_donation(end_date= nil)
    get_total_miles_for_months(end_date).sum {|m|m[:amount]} rescue 0.0
  end
  module ClassMethods
    def estimate_pledge_dates(fundraiser,donation_date=Date.today)
      start_date = donation_date
      # Set End date for calculation
      # End date default is today date 
      # if end date is greater than evenet_date+10.days then set event_date+10.days for processing donations
      # return @array of {:total_miles=>total_miles,:start_date=>month_start_date,:end_date=>month_end_date,:amount=>donation_amount}
      event_date = (fundraiser.event.happens_on+10.days).to_date
      end_date =  event_date ? event_date.to_date : end_date.to_date
      month_start_date,month_end_date = start_date,start_date
      month_miles = []
      begin
        month_end_date = month_end_date+1.month
        # If month end date is greater than end event date 
        month_end_date = month_end_date > event_date ? event_date : month_end_date
      
        month_miles << {:start_date=>month_start_date,:end_date=>month_end_date}
        # Update start date after 1 day for next iteration
        month_start_date = month_end_date+1.day
      end while (month_end_date < end_date)  
      month_miles
    end
  end
    
end