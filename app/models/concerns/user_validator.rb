module UserValidator
	extend ActiveSupport::Concern

	def validate_phone_number
		phone_regex = /\(?([0-9]{3})\)?([ -]?)([0-9]{3})([ -]?)\2([0-9]{4})/
		if self.phone_number.present?
			if (self.phone_number.match(phone_regex) && (self.phone_number.scan(/\d+/).join.length == 10))
				return true
			else
				self.errors[:base] << "Phone number is invalid."	
				return false
			end
		else
			self.errors[:base] << "Your Phone number can't be blank."
			return false
		end
	end
end