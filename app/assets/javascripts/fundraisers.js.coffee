# $('body').context_load "fundraiser", "show", () ->
#   $.ajaxSetup cache: true
#   $.getScript "//connect.facebook.net/en_UK/all.js", ->
#     FB.init appId: "YOUR_APP_ID"
#     $("#loginbutton,#feedbutton").removeAttr "disabled"
#     FB.getLoginStatus updateStatusCallback


$('body').context_ready ".fundraisers.new", ->
  class Fundraiser

    constructor: ->
      @setupSearch '#charity', "/charities.json", ['name', 'id', 'website', 'address', 'phone', 'image_url'], ".charities_container", charity_template, new_charity_link_template, 'charity', '#fundraiser_charity_id'
      @setupSearch '#event', "/events.json", ['name', 'id', 'happens_on'], ".events_container", event_template, new_event_link_template, 'event', '#fundraiser_event_id'
      

    setupSearch: (selector, ajax_url, attributes_for_template, output_container, jade_template, new_link_template, resource, resource_id_selector)->
      console.log 'setupSearch'
      console.log 'resource: ' + resource
      console.log 'resource_id_selector: ' + resource_id_selector
      instance = this

      $(selector).on 'keyup', ->

        if this.value == ''
          $(output_container).html ''
        else
          instance.addAjaxPending(output_container)

          search_box = this

          unless instance.ajaxQueue
            instance.ajaxQueue = setTimeout(->
              instance.sendAjaxSearch(selector, search_box.value, ajax_url, attributes_for_template, output_container, jade_template, new_link_template, resource, resource_id_selector)
              instance.ajaxQueue = null
            , 2000)


    sendAjaxSearch: (selector, query, url, attributes_for_template, output_container, jade_template, new_link_template, resource, resource_id_selector) =>
      instance = this
      $.get(url, { query: encodeURIComponent(query) }).done (data)->
        instance.generateView(selector, data, attributes_for_template, output_container, jade_template, new_link_template, resource, resource_id_selector)

    addAjaxPending: (output_container)->
      if $(output_container + ' .ajax_pending').length == 0
        console.log('ajax pending')
        $(output_container).prepend("<div class='ajax_pending'>Searching...</div>")

    generateView: (selector, results, attributes_for_template, output_container, jade_template, new_link_template, resource, resource_id_selector) =>
      instance = this
      $(output_container).removeClass('hide')
      $(selector).removeClass('hide')
      output_html = ""
      output_html += new_link_template()

      for result in results
        do (result) ->
          attributes_hash = {}
          for attribute in attributes_for_template 
            attributes_hash[attribute] = result[attribute]
          output_html += jade_template(attributes_hash)
          console.log attributes_hash
      $(output_container).html($(output_html))
      @setupResultSelector(output_container, resource, resource_id_selector)
      $('.new_resource_link').click (event)->
        event.preventDefault()
        console.log $(output_container).siblings('.new_resource')
        $(output_container).siblings('.new_resource').removeClass('hide')
        $(output_container).addClass('hide')
        $(selector).addClass('hide')


    setupResultSelector: (resource_container, resource, resource_id_selector)->
      instance = this
      $(resource_container + ' .' + resource + ' a.select').on 'click', (event)->
        event.preventDefault()
        $(resource_id_selector).val( $(this).data(resource + '-id') )
        $(resource_container + ' .' + resource).removeClass('selected')
        $(this).closest('.' + resource).addClass('selected')


    charity_template = jade.compile '''
      div(class='charity clearfix')
        if image_url
          div(class='logo')
            img(src=image_url width="150px" height="100px")
        else
          div(class='logo')
            img(src='/assets/charity-logo.jpg' width="150px" height="100px")    
        h2(class='name')= name
        p(class='address')= address
        if website
          p(class='website')
            a(href=website, class='website', target='_blank') Website
        a(href='#', data-charity-id=id, class= 'select btn btn-primary' ) Support This Charity
        
    '''

    event_template = jade.compile '''
      div(class='event')
        a(href='#', data-event-id= id, class= 'select btn btn-primary' ) I’m participating
        h2(class='name')= name
        div(class='happens_on')
          - month = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"]
          - d = new Date(happens_on)
          = month[(d.getMonth())] + ' '+ d.getUTCDate() +',' + d.getFullYear()
         
    '''

    new_charity_link_template = jade.compile '''
      p 
        = "Select a result from below. If you can’t find your charity, "
        
        a(href='#', class='new_resource_link')= "add a new charity"
        = ". Provide us with the name of the charity and any other information you have easily available. We’ll take care of the rest by reaching out to the charity and helping them create an account on Frundraise."
    '''


    new_event_link_template = jade.compile '''
      p 
        = "Select a result from below or "
        a(href='#', class='new_resource_link')=" add a new event"
        = " if you can't find your event."
    '''

  window.fundraiser = new Fundraiser
  console.log 'page loaded'
  
  