// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jade
//= require jquery_ujs
//= require jquery.ui.all
//= require best_in_place
//= require bootstrap
//= context_load
//= require highcharts
//= require highcharts/highcharts-more
//= require jquery.validate
//= require additional-methods
//= require_tree .
String.prototype.trim = String.prototype.trim || function () {
    return this.replace(/^\s+|\s+$/g, "");
};

String.prototype.trimLeft = String.prototype.trimLeft || function () {
    return this.replace(/^\s+/, "");
};

String.prototype.trimRight = String.prototype.trimRight || function () {
    return this.replace(/\s+$/, "");
};

String.prototype.trimFull = String.prototype.trimFull || function () {
    return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, "").replace(/\s+/g, " ");
};

$(document).ready(function() {
  $(".best_in_place").best_in_place();
  $('.datepicker').datepicker({minDate: '0',"dateFormat":"MM d, yy"});
  $(".validate_form").validate()
  $(".feedback_form").validate()

  $(".training_date").datepicker({maxDate: '0',"dateFormat":"mm/dd/yy"});
  $("#donation_type").on("change",function () {
    if($(this).val() == "fixed")
    {
      $("#pledge_donation_container").hide();
      $("#fixed_donation_container").addClass("show");
      $("#donation_per_mile_amount").val("");
      $("#donation_max_per_mile_amount").val("");
    }
    else{
      $("#fixed_donation_container").removeClass("show");
      $("#pledge_donation_container").show();
      $("#donation_fixed_amount").val("");
    }
  })
});
