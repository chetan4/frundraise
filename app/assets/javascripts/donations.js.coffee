$('body').context_ready ".donation.new", ->
  Logger =
    log: (log_entry)->
      if logging_enabled
        console.log log_entry

  NewDonationForm =
    initialize: ->
      Logger.log 'NewDonationForm.initialize'
      stripe_key = $('meta[name="stripe-key"]').attr('content')
      Stripe.setPublishableKey(stripe_key)
      @setup_form()
    
    # form: ->
    #   $('#new_donation')

    setup_form: ->
      Logger.log 'NewDonationForm.setup_form'
      $('#new_donation').on 'submit', (e)=>

        if typeof(e.originalEvent) != 'undefined'
          e.preventDefault()

          @disable_form()

          if @valid_form()
            @process_card()

    valid_form: ->
      Logger.log 'NewDonationForm.valid_form'
      if @terms_accepted() && @card_entered() && @handle_minimum_amount() && @handle_pledge_last_date() then true else false
    
    handle_pledge_last_date:->
      Logger.log 'NewDonationForm.handle_pledge_last_date'
      
      if($("#donation_type").val()=="pledge")
        lastDate = new Date($('#pledge_donation_last_date').data('donation-date'))
        cardDateString = $('#card_month').val()+"-28-"+$('#card_year').val()
        cardDate = new Date(cardDateString)
        if( cardDate < lastDate)
          $('.errors').html("Your card expires before the end of the fundraiser.</br> Please use a card that is valid for the entire fundraiser or make a fixed donation.")    
          $('.errors').removeClass('hide')
          @enable_form()
          false
        else
          true
      else
        true    
          

    handle_minimum_amount:->
      Logger.log 'NewDonationForm.handle_minimum_amount'
      if (((parseFloat($("#donation_max_per_mile_amount").val()).toFixed(2)=="NaN") || $("#donation_max_per_mile_amount").val() >= 0.50) && $("#donation_type").val()=="pledge")
        true
      else if ($("#donation_type").val()=="fixed" && $("#donation_fixed_amount").val()>=0.50) 
        true 
      else if($("#donation_type").val()=="pledge")
        $('.errors').html("The minimum amount is $0.50 for the maximum per month on your pledge.")    
        $('.errors').removeClass('hide')
        @enable_form()
        false
      else if($("#donation_type").val()=="fixed")  
        $('.errors').html("The minimum amount is $0.50.")    
        $('.errors').removeClass('hide')
        @enable_form()
        false

    card_entered: ->
      Logger.log 'NewDonationForm.card_entered'
      if $('#card_number').val().length
        true
      else
        @handle_card_not_entered()
        @enable_form()
        false

    handle_card_not_entered: ->
      Logger.log 'NewDonationForm.handle_card_not_entered'
      $('.errors').html("Please enter the credit card you would like to use for your donation.")
      $('.errors').removeClass('hide')

    terms_accepted: ->
      Logger.log 'NewDonationForm.terms_accepted'
      if ($("#donation_terms").is(':checked'))
        true
      else
        @handle_terms_not_accepted()
        @enable_form()
        false

    handle_terms_not_accepted: ->
      Logger.log 'NewDonationForm.handle_terms_not_accepted'
      $('.errors').html('Please accept the terms and conditions below.')
      $('.errors').removeClass('hide')

    process_card: ->
      Logger.log 'NewDonationForm.process_card'
      card =
        number: $('#card_number').val()
        expMonth: $('#card_month').val()
        expYear: $('#card_year').val()
        cvc: $('#card_security_code').val()
      Logger.log JSON.stringify(card)
      Stripe.card.createToken(card, @handle_stripe_response)

    disable_form: ->
      Logger.log 'NewDonationForm.disable_form'
      $('input[type=submit]').attr('disabled', true)

    enable_form: ->
      Logger.log 'NewDonationForm.enable_form'
      $('input[type=submit]').attr('disabled', false)

    handle_stripe_response: (status, response) ->
      current_this = this
      Logger.log 'NewDonationForm.handle_stripe_response status:' + status + ' response: ' + response

      if status == 200
        Logger.log("response.id: " + response.id)
        $('#donation_stripe_token').val(response.id)
        if($("#donation_type").val()=="pledge")
          window.new_donation.display_pledge_date();
          window.new_donation.enable_form()
        else  
          $('#new_donation').trigger('submit')
      else
        window.new_donation.handle_stripe_error_response(response)
        window.new_donation.enable_form()

    handle_stripe_error_response: (response)->
      $('.errors').html(response.error.message)
      $('.errors').removeClass('hide')

    display_pledge_date: =>  
      Logger.log 'NewDonationForm.display_pledge_date'
      $("#modal_pledge_date").modal('show')
      pledge_amount = parseFloat($("#donation_per_mile_amount").val()).toFixed(2);
      pledge_amount_text = "$"+pledge_amount 
      pledge_amount_text +=" per mile"
      if($("#donation_max_per_mile_amount").val()!="")
        pledge_max = parseFloat($("#donation_max_per_mile_amount").val()).toFixed(2);
        pledge_amount_text += ", $"+pledge_max 
        pledge_amount_text += " max per month"
      $(".modal_pledge_amount").text(pledge_amount_text)
      perMileAmount = parseFloat($("#donation_per_mile_amount").val())
      $("#no_max_amount_warning").hide()
      $("#max_amount_warning").hide()
      if(parseFloat($("#donation_max_per_mile_amount").val()).toFixed(2)=="NaN")  
        $("#no_max_amount_warning").show()
      if(perMileAmount>= 10)
        $("#max_amount_warning").show()
        
      window.new_donation.confirm_pledge_modal()

    confirm_pledge_modal: =>
      $("#confirm_pledge").on "click", (e)=>
        $('#new_donation').trigger('submit')


  logging_enabled = true

  window.new_donation = new Object(NewDonationForm)
  window.new_donation.initialize()